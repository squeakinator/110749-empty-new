﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="5" MadCap:lastHeight="2408" MadCap:lastWidth="1214" MadCap:fileTags="File Tags/FileTagSetStatus.Ready for Review">
    <head>
    </head>
    <body>
        <h1>Understanding <MadCap:variable name="PP-Glossary-Terms.Content Type" />s</h1>
        <p class="subtitle">XXX</p>
        <p>A <MadCap:variable name="PP-Glossary-Terms.Content Type" /> defines the structure of content users create from within <MadCap:variable name="PP-Corporate.Product" />. <MadCap:variable name="PP-Glossary-Terms.Content Type" />s enable users to more easily create  content that adheres to a defined structure, without having to worry about the output format of the content.</p>
        <p>Example <MadCap:variable name="PP-Glossary-Terms.Content Type" />s include:</p>
        <ul>
            <li>Press release</li>
            <li>Landing page</li>
            <li>Job listing</li>
            <li>News item</li>
        </ul>
        <p>Common to each of these (and other) <MadCap:variable name="PP-Glossary-Terms.Content Type" />s is a defined structure. </p>
        <p>Example structures for the <MadCap:variable name="PP-Glossary-Terms.Content Type" />s listed above: </p>
        <table style="width: 100%;">
            <col />
            <col />
            <tbody>
                <tr>
                    <td><MadCap:variable name="PP-Glossary-Terms.Content Type" />
                    </td>
                    <td>Structure</td>
                </tr>
                <tr>
                    <td>Press Release</td>
                    <td>Headline; Subhead, Body; Footer</td>
                </tr>
                <tr>
                    <td>Landing Page</td>
                    <td>Title; Overview; Details; Form Link</td>
                </tr>
                <tr>
                    <td>Job Listing</td>
                    <td>Title; Description; Requirements; Contact</td>
                </tr>
                <tr>
                    <td>News Item</td>
                    <td>Headline; Subhead; Body</td>
                </tr>
            </tbody>
        </table>
        <p>No matter how different the content of two press releases might be, it is common for their structures to be similar, if not identical. This is the basis for creating content that is based on templates when using a program like <MadCap:variable name="PP-Other-Brands.InDesign" />, and it is the basis for creating content that is based on <MadCap:variable name="PP-Corporate.Product" /> <MadCap:variable name="PP-Glossary-Terms.Content Type" />s.</p>
        <h2><MadCap:variable name="PP-Glossary-Terms.Strictly Structured" /> Content</h2>
        <p>The content created from within <MadCap:variable name="PP-Corporate.Product" /> is <MadCap:variable name="PP-Glossary-Terms.Strictly Structured" style="font-style: italic;" />, which means the content has an absolute structure that remains consistent. The <MadCap:variable name="PP-Glossary-Terms.Strictly Structured" /> content approach enables external systems to reliably access the content for publishing or further processing. </p>
        <p>Using the example above, an external system that publishes your press releases would know to expect a Headline, Subhead, Body and Footer in each release. Further, the boundaries between each content section would be unambiguous, so the system could more reliably publish the content without formatting errors.</p>
        <div class="note">
            <p><MadCap:variable name="PP-Glossary-Terms.Strictly Structured" /> content should not be confused with <i>structured content</i>, which describes the approach used with XML-based authoring, whereby content is surrounded by tags that define document structure. With <MadCap:variable name="PP-Glossary-Terms.Strictly Structured" /> content, each subsection of content is placed into a unique field, where it is isolated from surrounding content without the use of tags. </p>
        </div>
        <h2>Separating Content from Formatting</h2>
        <p>The goal of <MadCap:variable name="PP-Corporate.Product" />'s <MadCap:variable name="PP-Glossary-Terms.Strictly Structured" /> content approach is the creation of content that remains independent from formatting. </p>
        <p>This offers several benefits:</p>
        <ul>
            <li>Content can be more easily repurposed across publishing channels and devices because it can be formatted as needed</li>
            <li>Content remains usable after branding changes</li>
            <li>Users don't need to worry about formatting when creating the content</li>
            <li>Localization (translation)&#160;is easier because context is provided by the structure</li>
        </ul>
        <div class="note">
            <p>Concepts such as <i>content as a service</i> require that content not be formatted, so that it can be tailored the requirements of the licensee. (See <MadCap:snippetText src="../../Resources/Snippets/URL-Content-As-A-Service.flsnp" />.)</p>
        </div>
        <h2>Nesting <MadCap:variable name="PP-Glossary-Terms.Content Type" />s</h2>
        <p><MadCap:variable name="PP-Glossary-Terms.Content Type" />s can be nested, which enables you to think of them in a hierarchical manner. This is beneficial if you use a number of <MadCap:variable name="PP-Glossary-Terms.Content Type" />s that should share some information.</p>
        <p>For example, say you will use <MadCap:variable name="PP-Corporate.Product" /> to publish job listings, news items and partner listings. Each <MadCap:variable name="PP-Glossary-Terms.Content Type" /> is different, but there are a few pieces of information you want to make sure are included wherever the information is published:</p>
        <ul>
            <li>Copyright Notice</li>
            <li>Impressum Link</li>
        </ul>
        <p>These values could be added to a "Company Publishing" <MadCap:variable name="PP-Glossary-Terms.Content Type" /> that is selected as a "parent" to each new <MadCap:variable name="PP-Glossary-Terms.Content Type" /> created. The default values included in the "Company Publishing" <MadCap:variable name="PP-Glossary-Terms.Content Type" /> would then be included automatically with all other <MadCap:variable name="PP-Glossary-Terms.Content Type" />s created in <MadCap:variable name="PP-Corporate.Product" />. (See <MadCap:xref href="../Tasks/Content-Types-Create.htm">Creating [%=PP-Glossary-Terms.Content Type%]s</MadCap:xref>.)</p>
        <h2>Extending <MadCap:variable name="PP-Glossary-Terms.Content Type" />s</h2>
        <p>If a given piece of content requires more metadata than is provided by its assigned <MadCap:variable name="PP-Glossary-Terms.Content Type" />, additional <MadCap:variable name="PP-Glossary-Terms.Layer" />s can be added directly to any <MadCap:variable name="PP-Glossary-Terms.Content Record" /> that is based on the <MadCap:variable name="PP-Glossary-Terms.Content Type" />. This enables you to define a given piece of content with greater granularity, without affecting other <MadCap:variable name="PP-Glossary-Terms.Content Record" />s assigned to the same <MadCap:variable name="PP-Glossary-Terms.Content Type" />. </p>
        <p>For example, if the press release for a new product mentions an event at which the product will be introduced, you could add the Event <MadCap:variable name="PP-Glossary-Terms.Layer" /> to add the metadata fields that layer provides, which might include Event Name, Date and Location. </p>
    </body>
</html>